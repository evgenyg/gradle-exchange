class Drawer extends Backbone.View
  initialize: ->
    $("#newGroup").click @addGroup
    # HTML is our model
    @el.children("li").each (i,group) ->
      new Group
        el: $(group)
      $("li",group).each (i,project) ->
        new Project
          el: $(project)
  addGroup: ->
    r = jsRoutes.controllers.Projects.addGroup()
    $.ajax
      url: r.url
      type: r.type
      success: (data) ->
        _view = new Group
          el: $(data).appendTo("#projects")
        _view.el.find(".groupName").editInPlace("edit")
      error: (err) ->
        # TODO: Deal with

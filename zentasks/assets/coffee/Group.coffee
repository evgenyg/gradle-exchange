class Group extends Backbone.View
  events:
    "click    .toggle"          : "toggle"
    "click    .newProject"      : "newProject"
    "click    .deleteGroup"     : "deleteGroup"
  initialize: ->
    @id = @el.attr("data-group")
    @name = $(".groupName", @el).editInPlace
      context: this
      onChange: @renameGroup
  newProject: (e) ->
    e.preventDefault()
    @el.removeClass("closed")
    r = jsRoutes.controllers.Projects.add()
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        group: @el.attr("data-group")
      success: (tpl) ->
        _list = $("ul",@el)
        _view = new Project
          el: $(tpl).appendTo(_list)
        _view.el.find(".name").editInPlace("edit")
      error: (err) ->
        $.error("Error: " + err)
  deleteGroup: (e) ->
    e.preventDefault()
    false if (!confirm "Remove group and projects inside?")
    id = @el.attr("data-group-id")
    @loading(true)
    r = jsRoutes.controllers.Projects.deleteGroup(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      success: ->
        @el.remove()
        @loading(false)
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
  renameGroup: (name) =>
    @loading(true)
    r = jsRoutes.controllers.Projects.renameGroup(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        name: name
      success: (data) ->
        @loading(false)
        @name.editInPlace("close", data)
        @el.attr("data-group", data)
        @id = @el.attr("data-group")
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
  toggle: (e) ->
    e.preventDefault()
    @el.toggleClass("closed")
    false
  loading: (display) ->
    if (display)
      @el.children(".options").hide()
      @el.children(".loader").show()
    else
      @el.children(".options").show()
      @el.children(".loader").hide()

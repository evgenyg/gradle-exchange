class Tasks extends Backbone.View
  events:
    "click .newFolder"              : "newFolder"
    "click .list .action"           : "removeUser"
    "click .addUserList .action"    : "addUser"
  render: (project) ->
    @project = project
    # HTML is our model
    @folders = $.map $(".folder", @el), (folder) =>
      new TaskFolder
        el: $(folder)
        project: @project
  newFolder: (e) ->
    e.preventDefault()
    r = jsRoutes.controllers.Tasks.addFolder(@project)
    $.ajax
      url: r.url
      type: r.type
      context: this
      success: (tpl) ->
        newFolder = new TaskFolder
          el: $(tpl).insertBefore(".newFolder")
          project: @project
        newFolder.el.find("header > h3").editInPlace("edit")
      error: (err) ->
        $.error("Error: " + err)
    false
  removeUser: (e) ->
    e.preventDefault()
    r = jsRoutes.controllers.Projects.removeUser(@project)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        user: $(e.target).parent().data('user-id')
      success: ->
        $(e.target).parent().appendTo(".addUserList")
      error: (err) ->
        $.error("Error: " + err)
    false
  addUser: (e) ->
    e.preventDefault()
    r = jsRoutes.controllers.Projects.addUser(@project)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        user: $(e.target).parent().data('user-id')
      success: ->
        $(e.target).parent().appendTo(".users .list")
      error: (err) ->
        $.error("Error: " + err)
    false

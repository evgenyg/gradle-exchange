class Project extends Backbone.View
  events:
    "click      .delete"    : "deleteProject"
  initialize: ->
    @id = @el.attr("data-project")
    @name = $(".name", @el).editInPlace
      context: this
      onChange: @renameProject
  renameProject: (name) ->
    @loading(true)
    r = jsRoutes.controllers.Projects.rename(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        name: name
      success: (data) ->
        @loading(false)
        @name.editInPlace("close", data)
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
  deleteProject: (e) ->
    e.preventDefault()
    @loading(true)
    r = jsRoutes.controllers.Projects.delete(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      success: ->
        @el.remove()
        @loading(false)
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
    false
  loading: (display) ->
    if (display)
      @el.children(".delete").hide()
      @el.children(".loader").show()
    else
      @el.children(".delete").show()
      @el.children(".loader").hide()

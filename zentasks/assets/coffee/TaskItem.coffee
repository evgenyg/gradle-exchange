class TaskItem extends Backbone.View
  events:
    "change .done"          : "onToggle"
    "click .deleteTask"     : "deleteTask"
    "dblclick h4"           : "editTask"
  initialize: (options) ->
    @check = @el.find(".done")
    @id = @el.attr("data-task-id")
    @folder = options.folder
  deleteTask: (e) =>
    e.preventDefault() if e?
    @loading(false)
    r = jsRoutes.controllers.Tasks.delete(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        name: name
      success: (data) ->
        @loading(false)
        @el.remove()
        @trigger("delete", @)
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
    false
  editTask: (e) =>
    e.preventDefault()
    # TODO
    alert "not implemented yet."
    false
  toggle: (val) =>
    @loading(true)
    r = jsRoutes.controllers.Tasks.update(@id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        done: val
      success: (data) ->
        @loading(false)
        @check.attr("checked",val)
        @trigger("change", @)
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
  onToggle: (e) =>
    e.preventDefault()
    val = @check.is(":checked")
    log val
    @toggle(val)
    false
  loading: (display) ->
    if (display)
      @el.find(".delete").hide()
      @el.find(".loader").show()
    else
      @el.find(".delete").show()
      @el.find(".loader").hide()

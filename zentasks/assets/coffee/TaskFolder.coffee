class TaskFolder extends Backbone.View
  events:
    "click .deleteCompleteTasks"    : "deleteCompleteTasks"
    "click .deleteAllTasks"         : "deleteAllTasks"
    "click .deleteFolder"           : "deleteFolder"
    "change header>input"           : "toggleAll"
    "submit .addTask"               : "newTask"
  initialize: (options) =>
    @project = options.project
    @tasks = $.map $(".list li",@el), (item)=>
      newTask = new TaskItem
        el: $(item)
        folder: @
      newTask.bind("change", @refreshCount)
      newTask.bind("delete", @deleteTask)
    @counter = @el.find(".counter")
    @id = @el.attr("data-folder-id")
    @name = $("header > h3", @el).editInPlace
      context: this
      onChange: @renameFolder
    @refreshCount()
  newTask: (e) =>
    e.preventDefault()
    $(document).focus() # temporary disable form
    form = $(e.target)
    taskBody = $("input[name=taskBody]", form).val()
    r = jsRoutes.controllers.Tasks.add(@project, @id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        title: $("input[name=taskBody]", form).val()
        dueDate: $("input[name=dueDate]", form).val()
        assignedTo:
          email: $("input[name=assignedTo]", form).val()
      success: (tpl) ->
        newTask = new TaskItem(el: $(tpl), folder: @)
        @el.find("ul").append(newTask.el)
        @tasks.push(newTask)
        form.find("input[type=text]").val("").first().focus()
      error: (err) ->
        alert "Something went wrong:" + err
    false
  renameFolder: (name) =>
    @loading(true)
    r = jsRoutes.controllers.Tasks.renameFolder(@project, @id)
    $.ajax
      url: r.url
      type: r.type
      context: this
      data:
        name: name
      success: (data) ->
        @loading(false)
        @name.editInPlace("close", data)
        @el.attr("data-folder-id", data)
        @id = @el.attr("data-folder-id")
      error: (err) ->
        @loading(false)
        $.error("Error: " + err)
  deleteCompleteTasks: (e) =>
    e.preventDefault()
    $.each @tasks, (i, item) ->
      item.deleteTask() if item.el.find(".done:checked").length > 0
      true
    false
  deleteAllTasks: (e) =>
    e.preventDefault()
    $.each @tasks, (i, item)->
      item.deleteTask()
      true
    false
  deleteFolder: (e) =>
    e.preventDefault()
    @el.remove()
    false
  toggleAll: (e) =>
    val = $(e.target).is(":checked")
    $.each @tasks, (i, item) ->
      item.toggle(val)
      true
  refreshCount: =>
    count = @tasks.filter((item)->
      item.el.find(".done:checked").length == 0
    ).length
    @counter.text(count)
  deleteTask: (task) =>
    @tasks = _.without @tasks, tasks
    @refreshCount()
  loading: (display) ->
    if (display)
      @el.find("header .options").hide()
      @el.find("header .loader").show()
    else
      @el.find("header .options").show()
      @el.find("header .loader").hide()

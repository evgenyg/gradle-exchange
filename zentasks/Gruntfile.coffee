module.exports = ( grunt ) ->
  grunt.initConfig
    pkg: grunt.file.readJSON( 'package.json' )

    # -----------------------------------------------
    # https://github.com/gruntjs/grunt-contrib-clean
    # -----------------------------------------------

    clean:
      build: [ 'public/js/app.js', 'public/js/app.min.js', 'public/js/lib.min.js', 'public/css/main.css', 'public/css/login.css', 'public/css/main.min.css', 'public/css/login.min.css' ]

    # -----------------------------------------------
    # https://github.com/gruntjs/grunt-contrib-coffee
    # -----------------------------------------------

    coffee:
      compileJoined:
        options:
          join     : true
          sourceMap: true
        files:
          'public/js/app.js' : [ 'assets/coffee/*.coffee' ]
        

    # -----------------------------------------------
    # https://github.com/gruntjs/grunt-contrib-uglify
    # -----------------------------------------------

    uglify:
      options:
        mangle: false
      build:
        files:
          'public/js/app.min.js' : [ 'public/js/app.js' ]
          'public/js/lib.min.js' : [ 'assets/lib/jquery-1.7.1.js', 'assets/lib/jquery-play-1.7.1.js', 'assets/lib/underscore-min.js', 'assets/lib/backbone-min.js' ]
      

    # -----------------------------------------------
    # https://github.com/gruntjs/grunt-contrib-less
    # -----------------------------------------------

    less:
      build:
        options:
          yuicompress: false
        files:
          'public/css/main.css' : [ 'assets/less/libs/_reset.less', 'assets/less/libs/_mate.less', 'assets/less/libs/_theme.less', 'assets/less/main.less', 'assets/less/main/_layout.less', 'assets/less/main/_widgets.less', 'assets/less/main/_drawer.less', 'assets/less/main/_header.less', 'assets/less/main/_breadcrumb.less', 'assets/less/main/_tasks.less/*.css' ]
          'public/css/login.css' : [ 'assets/less/libs/_reset.less', 'assets/less/libs/_mate.less', 'assets/less/libs/_theme.less', 'assets/less/login.less', 'assets/less/main/_widgets.less', 'assets/less/main/_header.less' ]
        
      buildMinified:
        options:
          yuicompress: true
        files:
          'public/css/main.min.css' : [ 'public/css/main.css' ]
          'public/css/login.min.css' : [ 'public/css/login.css' ]
        

  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.registerTask 'default', [ 'clean', 'coffee', 'uglify', 'less' ]

